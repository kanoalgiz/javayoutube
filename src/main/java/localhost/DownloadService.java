package localhost;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class DownloadService {

  private static final String YOUTUBE_VIDEO_URI = "https://www.youtube.com/watch?v=";

  public void download(Video video, String filename)
      throws IOException, InterruptedException {

    String videoUrl = YOUTUBE_VIDEO_URI + video.getId();

    String EXEC_LOCATION = DownloadService.class.getClassLoader().getResource("youtube-dl.exe")
        .getFile();

    long songMiddle = video.getDuration() / 1000 / 2;
    String start = Long.toString(songMiddle - 30);

    Process process = new ProcessBuilder()
        .command(EXEC_LOCATION, "\"" + videoUrl + "\"", "--audio-format", "mp3", "-x",
            "--audio-quality", "0", "-o", "results\\" + filename + ".%(ext)s",
            "--postprocessor-args", "-ss " + start + " -t 60")
        .start();

    BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
    String line = "";
    while (line != null) {
     // System.out.println(line);
      line = reader.readLine();
    }

    int waitFlag = process.waitFor();
    reader.close();

    if(waitFlag != 0) {
      System.out.println("Process returned '" + waitFlag + "' code!");
      throw new IOException();
    }

    File resultingMp3 = new File("results\\" + filename + ".mp3");
    if(resultingMp3.length() < 1_048_576) {
      System.out.println("Resulting file is less then a megabyte, there must be a problem with conversion!");
      resultingMp3.delete();
      throw new IOException();
    }
  }
}
