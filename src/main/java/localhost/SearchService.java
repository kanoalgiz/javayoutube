package localhost;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class SearchService {

  private static final String BASE_YOUTUBE_URL = "https://www.youtube.com/results?search_query=";

  public List<Video> findVideos(String query) {

    String result = "";
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpGet httpget = new HttpGet(
          BASE_YOUTUBE_URL + URLEncoder.encode(query, StandardCharsets.UTF_8));
      try (final CloseableHttpResponse response = httpclient.execute(httpget)) {
        HttpEntity entity = response.getEntity();

        if (entity != null) {
          result = new BufferedReader(new InputStreamReader(entity.getContent()))
              .lines()
              .collect(Collectors.joining("\n"));
        }
      }
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }

    result = StringEscapeUtils.unescapeHtml4(result);

    return parseVideosFromPage(result);
  }

  private List<Video> parseVideosFromPage(String page) {

    List<Video> videos = new ArrayList<>();

    while (page.contains("/watch?v=") && videos.size() < 20) {
      try {
        page = page.substring(page.indexOf("/watch?v=") + 9);
        String id = page.substring(0, 11);

        String sub = page.substring(page.indexOf("title=\"") + 7);
        String title = sub.substring(0, sub.indexOf("\""));
        if (title.contains("позже")) {
          continue;
        }

        sub = sub.substring(sub.indexOf("Продолжительность:"));
        String duration = sub.substring(sub.indexOf(": ") + 2, sub.indexOf("<"));

        sub = sub.substring(sub.indexOf("data-sessionlink="));
        String channelName = sub.substring(sub.indexOf(">") + 1, sub.indexOf("<"));

        //System.out.print("\nParsed: " + id + " " + title + " " + duration + " " + channelName);

        Video video = Video.builder()
            .id(id)
            .title(title.toLowerCase())
            .channelName(channelName.toLowerCase())
            .duration(parseDuration(duration))
            .build();

        videos.add(video);
      } catch (StringIndexOutOfBoundsException ignored) {
      }
    }

    return videos;
  }

  public Long parseDuration(String duration) {

    long minutes = Long.parseLong(duration.split(":")[0]);
    long seconds = Long.parseLong(duration.split(":")[1]);

    return (minutes * 60 * 1000) + (seconds * 1000);
  }
}
