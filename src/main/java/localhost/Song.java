package localhost;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Song {

  private String artistName;

  private String trackName;

  private String danceability;

  private long duration;

  private String energy;

  private String valence;

  public boolean validateDuration() {
    if (duration < 2 * 60 * 1000) {
      System.out.println(
          "Filtered " + artistName + " - " + trackName + " because it is shorter than 2 minutes");
      return false;
    }
    return true;
  }
}
